using UnityEngine;

[RequireComponent(typeof(RectTransform))]
public class SafeArea : MonoBehaviour
{
    private RectTransform _rectTransform;
    [SerializeField] private bool ignoreBottom;

    private void Awake()
    {
        _rectTransform = GetComponent<RectTransform>();
        if (!ignoreBottom)
        {
            var safeArea = Screen.safeArea;
            var anchorMin = safeArea.position;
            var anchorMax = anchorMin + safeArea.size;

            anchorMin.x /= Screen.width;
            anchorMin.y /= Screen.height;
            anchorMax.x /= Screen.width;
            anchorMax.y /= Screen.height;

            _rectTransform.anchorMin = anchorMin;
            _rectTransform.anchorMax = anchorMax;
        }
        else
        {
            // If ignoring bottom safe area, set anchors to cover the entire screen except the bottom
            _rectTransform.anchorMin = Vector2.zero;
            _rectTransform.anchorMax = new Vector2(1f, 1f - Screen.safeArea.yMin / Screen.height);
        }
    }
}