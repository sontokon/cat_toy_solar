using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.EventSystems;
using Random = UnityEngine.Random;

public class Mouse : MonoBehaviour
{
    public float rotationModifier;
    
    [SerializeField] private float mouseSpeed;
    [SerializeField] private float rotationSpeed;

    public Transform mouseSprite;
    
    
    private Rigidbody2D rb;
    private Vector3 targetPosition;
    private Vector3 moveDirection;
    private Vector3 mouseDragStartPosition;
    private Vector3 spriteDragStartPosition;
    

    private bool canRun;
    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        SetUpMouseSpeed(SettingManager.instance._difficultyType);
        canRun = true;
        SetNewTargetPosition();
    }
    
    private void FixedUpdate()
    {
        MouseMoving();
    }

    private void SetNewTargetPosition()
    {
        Camera mainCamera = Camera.main;
        if (mainCamera == null)
        {
            Debug.LogError("Main camera is not found!");
            return;
        }

        float xMin = mainCamera.ViewportToWorldPoint(new Vector3(0, 0, 0)).x;
        float xMax = mainCamera.ViewportToWorldPoint(new Vector3(1, 0, 0)).x;
        float yMin = mainCamera.ViewportToWorldPoint(new Vector3(0, 0, 0)).y;
        float yMax = mainCamera.ViewportToWorldPoint(new Vector3(0, 1, 0)).y;

        float xPos = Random.Range(xMin, xMax);
        float yPos = Random.Range(yMin, yMax);

        targetPosition = new Vector3(xPos, yPos, 0f);
        moveDirection = (targetPosition - transform.position).normalized;
    }

    public void MouseMoving()
    {
        if (canRun)
        {
            rb.MovePosition(rb.position + (Vector2)moveDirection * mouseSpeed * Time.deltaTime);

            Debug.DrawLine(transform.position, transform.position + moveDirection, Color.blue);
            if (Vector3.Distance(transform.position, targetPosition) < 0.5f)
            {
                SetNewTargetPosition();
            }
            Vector2 direction = (targetPosition - transform.position).normalized;
            float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg - rotationModifier;
            Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);
            transform.rotation = Quaternion.Slerp(transform.rotation, q, Time.deltaTime * rotationSpeed); 
        }
        else
        {
            return;
        }

    }

    public void OnMouseDown()
    {
        canRun = false;
        mouseDragStartPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        spriteDragStartPosition = transform.localPosition;
        transform.DOShakeRotation(1, new Vector3(0,0,10), 29, 90, true, ShakeRandomnessMode.Full);
    }

    public void OnMouseDrag()
    {
        transform.localPosition = spriteDragStartPosition +
                                  (Camera.main.ScreenToWorldPoint(Input.mousePosition) - mouseDragStartPosition);
    }

    public void OnMouseUp()
    {
        canRun = true;
        SetNewTargetPosition();
    }

    public void SetUpMouseSpeed(Difficulty difficulty)
    {
        switch (difficulty)
        {
            case Difficulty.Easy:
                mouseSpeed = 1f;
                break;
            case Difficulty.Medium:
                mouseSpeed = 2f;
                break;
            case Difficulty.Hard:
                mouseSpeed = 4f;
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(difficulty), difficulty, null);
        }
    }
}