using System;using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;
using Utils;

public enum GameMode
{
    Mouse,
    Fish,
    Feather,
    Cockroach,
    Spider,
    Firefly,
    Laser,
    Bat,
    Gecko,
    Finger
}
public class GameUIManager : Singleton<GameUIManager>
{
    public GameObject gamePlayCanvas;
    public Image bgGameplay;
    public Sprite[] gameSprites;

    [Header("Pause")] public GameObject pausePanel;
    
    public void SetGameplayBG(GameMode mode)
    {
        bgGameplay.sprite = gameSprites[(int) mode];
        gamePlayCanvas.SetActive(true);
    }

    public void OnClickPause()
    {
        pausePanel.SetActive(true);
    }

    public void OnReturnMenu()
    {
        pausePanel.SetActive(false);
        gamePlayCanvas.SetActive(false);
        GameController.instance.Hide();
    }

    public void OnResumeGame()
    {
        pausePanel.SetActive(false);
    }
}
