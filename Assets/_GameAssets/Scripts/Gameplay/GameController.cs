using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils;

public class GameController : Singleton<GameController>
{
    public Camera mainCamera;

    [SerializeField] private List<GameObject> gamePrefabs;
    
    public void CheckCharacterPosition(Vector3 charPosition)
    {
        if (mainCamera == null)
        {
            Debug.LogError("Main camera is not assigned!");
        }

        Vector3 screenPoint = mainCamera.WorldToViewportPoint(charPosition);

        if (screenPoint.x < 0 || screenPoint.x > 1 || screenPoint.y < 0 || screenPoint.y > 1)
        {
            print("out of border destroy gameobject");
        }
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }

    public void Show(int index)
    {
        gameObject.SetActive(true);
        Instantiate(gamePrefabs[index], this.transform);
    }
}
