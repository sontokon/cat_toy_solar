using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class MenuController : Utils.Singleton<MenuController>
{
    [Header("Toy/Whistle")] public GameObject[] menuPanel;
    [Header("SettingPanel")] public GameObject settingPanel;
    [SerializeField] private int gameIndex;
    public GameMode _mode;

    private void Start()
    {
        ChangePanel(0);
    }

    public void ChangePanel(int index)
    {
        switch (index)
        {
            case 0:
                menuPanel[0].SetActive(true);
                menuPanel[1].SetActive(false);
                break;
            case 1:
                menuPanel[0].SetActive(false);
                menuPanel[1].SetActive(true);
                break;
        }
    }

    public void OpenSettingPanel(int gameIndex)
    {
        this.gameIndex = gameIndex;
        SetGameMode(gameIndex);
        print(gameIndex);
        settingPanel.SetActive(true);
    }

    public void SetGameMode(int gameIndex)
    {
        switch (gameIndex)
        {
            case 0:
                _mode = global::GameMode.Mouse;
                break;
            case 1:
                _mode = global::GameMode.Fish;
                break;
            case 2:
                _mode = global::GameMode.Feather;
                break;
            case 3:
                _mode = global::GameMode.Cockroach;
                break;
            case 4:
                _mode = global::GameMode.Spider;
                break;
            case 5:
                _mode = global::GameMode.Firefly;
                break;
            case 6:
                _mode = global::GameMode.Laser;
                break;
            case 7:
                _mode = global::GameMode.Bat;
                break;
            case 8:
                _mode = global::GameMode.Gecko;
                break;
            case 9:
                _mode = global::GameMode.Finger;
                break;
        }
    }

    public void CloseSettingPanel()
    {
        settingPanel.SetActive(false);
    }

    public void OnClickPlay()
    {
        print(_mode);
        SettingManager.instance.OnChangeLevelSetting();
        GameUIManager.instance.SetGameplayBG(_mode);
        GameController.instance.Show(gameIndex);
        settingPanel.SetActive(false);
    }
}