using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor.UI;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ToggleSwitch : MonoBehaviour, IPointerClickHandler
{
    [Header("Slider Setup")] [SerializeField, Range(0, 1f)]
    private float sliderValue;
    
    public bool CurrentValue { get; private set; }

    private Slider _slider;

    [Header("Animation")] [SerializeField, Range(0, 1f)]
    private float animationDuration = 0.5f;

    [SerializeField] private AnimationCurve sliderEase =
        AnimationCurve.EaseInOut(0, 0, 1, 1);

    private Coroutine _animateSliderCoroutine;

    [Header("Events")] 
    [SerializeField] private UnityEvent onToggleOn;
    [SerializeField] private UnityEvent onToggleOff;

     private ToggleSwitchGroupManager _toggleSwitchGroupManager;
    private void OnValidate()
    {
        SetUpToggleComponents();
        _slider.value = sliderValue;
    }

    private void SetUpToggleComponents()
    {
        if (_slider != null)
        {
            return;
        }

        SetUpSliderComponents();
    }

    private void SetUpSliderComponents()
    {
        _slider = GetComponent<Slider>();
        if (_slider == null)
        {
            Debug.Log("No Slider Found",this);
            return;
        }

        _slider.interactable = false;
        var sliderColor = _slider.colors;
        sliderColor.disabledColor = Color.white;
        _slider.colors = sliderColor;
        _slider.transition = Selectable.Transition.None;

    }

    public void SetupForManager(ToggleSwitchGroupManager manager)
    {
        _toggleSwitchGroupManager = manager;
    }

    private void Awake()
    {
        SetUpToggleComponents();
    }
    public void OnPointerClick(PointerEventData eventData)
    {
        Toggle();
    }

    private void Toggle()
    {
        if (_toggleSwitchGroupManager != null)
        {
            _toggleSwitchGroupManager.ToggleGroup(this);
        }
        else
        {
            SetStateAndStartAnimation(!CurrentValue);
        }
    }

    public void ToggleByGroupManager(bool valueToSetTo)
    {
        SetStateAndStartAnimation(valueToSetTo);
    }

    private void SetStateAndStartAnimation(bool state)
    {
        CurrentValue = state;
        if (CurrentValue)
        {
            onToggleOn?.Invoke();
        }
        else
        {
            onToggleOff?.Invoke();
        }

        if (_animateSliderCoroutine != null)
        {
            StopCoroutine(_animateSliderCoroutine);
        }

        _animateSliderCoroutine = StartCoroutine(AnimateSlider());
    }

    IEnumerator AnimateSlider()
    {
        float startValue = _slider.value;
        float endValue = CurrentValue ? 1 : 0;
        float time = 0;
        if (animationDuration > 0)
        {
            while (time< animationDuration)
            {
                time += Time.deltaTime;

                float lerpFactor = sliderEase.Evaluate(time / animationDuration);
                _slider.value = sliderValue = Mathf.Lerp(startValue, endValue, lerpFactor);
                yield return null;
            }
            
        }

        _slider.value = endValue;
    }

    public void TurnOnSound()
    {
        print("turn on");
    }

    public void TurnOffSound()
    {
        print("turn off");
    }

    public void TurnOnVibrate()
    {
        print("turn on vibrate");
    }

    public void TurnOffVibrate()
    {
        print("turn off vibrate");
    }

    public void TurnOnPhotoMode()
    {
        print("turn on photo mode");
    }

    public void TurnOffPhotoMode()
    {
        print("turn off photo mode");
    }
    
   
}
