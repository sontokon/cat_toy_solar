using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;
using Utils;

public enum Difficulty
{
    Easy,
    Medium,
    Hard,
}
public class SettingManager : Singleton<SettingManager>
{
    [SerializeField]private Slider levelDifficulty;
    public Difficulty _difficultyType { get; private set; }

    [Header("Text")] 
    public TMP_Text easyText;
    public TMP_Text mediumText;
    public TMP_Text hardText;

    [Header("Color")] 
    public Color deActiveColor;
    public Color activeColor;

     private void Start()
     {
         OnChangeLevelSetting();
     }

     public void OnChangeLevelSetting()
    {
        float value = levelDifficulty.value;

        if (value >= 0.6f)
        {
            _difficultyType = Difficulty.Hard;
            levelDifficulty.value = 1f; 
        }
        else if (value >= 0.4f)
        {
            _difficultyType = Difficulty.Medium;
            levelDifficulty.value = 0.5f;
        }
        else
        {
            _difficultyType = Difficulty.Easy;
            levelDifficulty.value = Mathf.Lerp(levelDifficulty.value, 0, 2);
            levelDifficulty.value = 0;
        }
        SetTextOnChangeLevel(_difficultyType);
        print(_difficultyType);
    }

    public void SetTextOnChangeLevel(Difficulty difficulty)
    {
        switch (difficulty)
        {
            case Difficulty.Easy:
                easyText.color = activeColor;
                mediumText.color = deActiveColor;
                hardText.color = deActiveColor;
                easyText.fontSize = 45;
                mediumText.fontSize = 40;
                hardText.fontSize = 40;
                break;
            case Difficulty.Medium:
                easyText.color = deActiveColor;
                mediumText.color = activeColor;
                hardText.color = deActiveColor;
                easyText.fontSize = 40;
                mediumText.fontSize = 45;
                hardText.fontSize = 40;
                break;
            case Difficulty.Hard:
                easyText.color = deActiveColor;
                mediumText.color = deActiveColor;
                hardText.color = activeColor;
                easyText.fontSize = 40;
                mediumText.fontSize = 40;
                hardText.fontSize = 45;
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(difficulty), difficulty, null);
        }
        
    }
}
