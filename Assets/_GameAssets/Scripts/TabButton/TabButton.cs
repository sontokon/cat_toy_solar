using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class TabButton : MonoBehaviour, IPointerClickHandler
{
    public TabGroup tabGroup;

    public Image background;
    public Sprite selectSprite;
    public Sprite deSelectSprite;
   // public Sprite displayBigImage;
    public int gameIndex;
    public GameMode mode;
    public string gameNameText;
    private void Start()
    {
        background = GetComponent<Image>();
        tabGroup.Subcribe(this);
        if (gameIndex == 0)
        {
            background.sprite = selectSprite;
        }
    }

    /*
    public void OnPointerEnter(PointerEventData eventData)
    {
        tabGroup.OnTabEnter(this);
    }
    */

    public void OnPointerClick(PointerEventData eventData)
    {
        tabGroup.OnTabSelected(this);
        MenuController.instance.ChangePanel(gameIndex);
    }

    /*public void OnPointerExit(PointerEventData eventData)
    {
        tabGroup.OnTabExit(this);
        background.sprite = deSelectSprite;
    }*/
}
