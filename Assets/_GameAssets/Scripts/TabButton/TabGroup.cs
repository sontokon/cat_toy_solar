using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TabGroup : MonoBehaviour
{
    public  List<TabButton> tabButtons;
    public TabButton selectedTab;
    
    public void Subcribe(TabButton button)
    {
        if (tabButtons == null)
        {
            tabButtons = new List<TabButton>();
        }
        tabButtons.Add(button);
            
    }

    /*public void OnTabEnter(TabButton button)
    {
        
    }*/
    public void OnTabSelected(TabButton button)
    {
        selectedTab = button;
        ResetTabs();
        button.background.sprite = button.selectSprite;
    }

    /*
    public void OnTabExit(TabButton button)
    {
        ResetTabs();
    }
    */

    public void ResetTabs()
    {
        foreach (TabButton button in tabButtons)
        {
            if(selectedTab != null && button == selectedTab){continue;}
            button.background.sprite = button.deSelectSprite; 
        }
    }
    
}
